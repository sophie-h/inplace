use jpeg_inplace::EXIF_IDENTIFIER_STRING;

#[test]
fn exif() {
    let data = std::fs::read("exif-xmp.jpg").unwrap();
    let jpeg = jpeg_inplace::Jpeg::new(&data);

    let exif = exif_inplace::Exif::new(jpeg.exif_data().next().unwrap().to_vec()).unwrap();

    assert_eq!(
        exif.orientation(),
        Some(inplace_common::orientation::Orientation::Id)
    );

    assert_eq!(exif.model(), Some(String::from("iPhone 6")));
}

#[test]
fn xmp() {
    let data = std::fs::read("exif-xmp.jpg").unwrap();
    let jpeg = jpeg_inplace::Jpeg::new(&data);

    let xmp = xmp_inplace::Xmp::new(jpeg.xmp_data().next().unwrap().to_vec()).unwrap();

    assert_eq!(
        xmp.get(&(xmp_inplace::Tag::Xmp, "CreatorTool".into())),
        Some("GIMP 2.10")
    );
}

#[test]
fn rotate() {
    let mut data = std::fs::read("exif-xmp.jpg").unwrap();

    let jpeg = jpeg_inplace::Jpeg::new(&data);
    let mut exif = exif_inplace::internal::ExifRaw::new(jpeg.exif_data().next().unwrap().to_vec());

    exif.decode().unwrap();
    let entry = exif
        .lookup_entry(inplace_common::field::Orientation)
        .unwrap();

    let pos = jpeg.exif().next().unwrap().data_pos() as usize
        + entry.value_offset_position() as usize
        + EXIF_IDENTIFIER_STRING.len();

    let current_orientation =
        inplace_common::orientation::Orientation::try_from(data[pos] as u16).unwrap();

    let new_rotation = current_orientation.rotate() + inplace_common::orientation::Rotation::_180;

    let new_orientation =
        inplace_common::orientation::Orientation::new(new_rotation, current_orientation.mirror());

    drop(jpeg);
    data[pos] = new_orientation as u8;

    let jpeg = jpeg_inplace::Jpeg::new(&data);
    let exif = exif_inplace::Exif::new(jpeg.exif_data().next().unwrap().to_vec()).unwrap();
    assert_eq!(
        exif.orientation(),
        Some(inplace_common::orientation::Orientation::Rotation180)
    );
}
