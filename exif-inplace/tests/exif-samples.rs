use std::ffi::OsStr;

use exif_inplace::internal::ExifRaw;
use jpeg_inplace::Jpeg;

#[test]
fn all_jpgs() {
    let dirs = [
        "tests/exif-samples/jpg",
        "tests/exif-samples/jpg/exif-org",
        "tests/exif-samples/jpg/gps",
        "tests/exif-samples/jpg/mobile",
        "tests/exif-samples/jpg/orientation",
        "tests/exif-samples/jpg/tests",
    ];

    for dir in dirs {
        for entry in std::fs::read_dir(dir).unwrap() {
            let entry = entry.unwrap();
            if entry.path().extension() == Some(OsStr::new("jpg")) {
                load_file(&entry.path());
            }
        }
    }
}

fn load_file(path: &std::path::Path) {
    dbg!(path);
    let image_data = std::fs::read(path).unwrap();

    let image = Jpeg::new(&image_data);

    let mut iter = image.exif_data();
    if let Some(exif_raw) = iter.next() {
        let mut decoder = ExifRaw::new(exif_raw.to_vec());
        decoder.decode().unwrap();
    } else {
        dbg!("Skipping.");
    }
}
